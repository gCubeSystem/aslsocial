
# Changelog for ASL Social

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.8.1] - 2023-05-14

 - EmailNotificationsConsumer set debug false
 - Bug 27457: get application profile multiple times before settling on sitelandingpath


## [v1.8.0] - 2022-05-05

 - fixed some notification methods

## [v1.0.0] - 2016-06-01

 - First release 
