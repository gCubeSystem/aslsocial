package org.gcube.applicationsupportlayer.social.ex;

@SuppressWarnings("serial")
public class UnsupportedNotificationException extends Exception {
	 public UnsupportedNotificationException(String message) {
	    super(message);
	  }
}
